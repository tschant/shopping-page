import {Product} from '../types/product';

const fetchProducts = (): Array<Product> => {
	let products: Array<Product> = require('./products.json');
	return products;
};

export default {
	fetchProducts
};
