import { InferGetServerSidePropsType } from 'next';
import Head from 'next/head'
import { Box, Container, Text } from '@chakra-ui/layout'
import styles from '../styles/Home.module.css'
import ProductGrid from './components/ProductGrid'
import api from '../lib/api';
import type {Product, SelectedProduct} from '../types/product';
import {Grid, Stack, Tag} from '@chakra-ui/react';
import {useState} from 'react';
import {CloseIcon} from '@chakra-ui/icons';
export const getServerSideProps = async () => {
	// TODO: paging, loading is slow for ALL (move categories to API/BE)
	const products: Array<Product> = api.fetchProducts() || [];
	const productCategories: Array<string> = products.map(({category}) => category).filter((category, index, self) => Boolean(category) && index === self.indexOf(category));

	return {
		props: {
			products,
			productCategories,
		}
	};
};

function Home({products, productCategories}: InferGetServerSidePropsType<typeof getServerSideProps>) {
	const [categoryFilter, setCategoryFilter] = useState('');
	const [selectedProducts, setSelectedProducts]: [Array<SelectedProduct>, Function] = useState([]);
	const findProductName = (selected: SelectedProduct) => {
		const found = products.find(prod => prod.productId.value === selected.productId);
		return found ? found.name : '';
	};

	// TODO: updating quantity logic is not working properly
	return (
		<div className={styles.container}>
			<Head>
				<title>Shopping Page</title>
				<meta name="description" content="Shopping Page" />
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<Container maxW="container.xl" overflow="hidden">
				<Stack direction="row" w="90%">
					<Box>
						<Box>
							<Text fontWeight="bold">Shop by category</Text>
							<Grid autoColumns="max-content" autoFlow="column" gap={6}>
								{ productCategories && productCategories.map(category => {
									return (<Tag size="lg" cursor="pointer" variant={category === categoryFilter ? "solid" : "outline"} key={category} onClick={() => setCategoryFilter(category)}>
										{category}
									</Tag>);
								}) }
								{categoryFilter && (<Tag size="lg" cursor="pointer" variant="outline" onClick={() => setCategoryFilter('')}><CloseIcon h={3} w={3}></CloseIcon>&nbsp;Clear filter </Tag>)}
							</Grid>
						</Box>
						<Box maxH="calc(100% - 10px)" overflowY="scroll">
							<ProductGrid products={products.filter(product => categoryFilter ? product.category === categoryFilter : true)}
								setSelected={(newSelectedProducts: Array<SelectedProduct>) => setSelectedProducts(newSelectedProducts)}>
							</ProductGrid>
						</Box>
					</Box>

					<Box w="5%">
						{selectedProducts && selectedProducts.map(selected => (
							<Text textOverflow="ellipsis" whiteSpace="nowrap">{selected.quantity} {findProductName(selected)} </Text>
						))}
					</Box>
				</Stack>
			</Container>
		</div>
	)
}

export default Home

