import { Box, Text } from '@chakra-ui/layout';
import { Button, Center, Stack } from '@chakra-ui/react';
import {AddIcon, MinusIcon, PlusSquareIcon} from '@chakra-ui/icons';
import { Image } from "@chakra-ui/image";
import type {Product, SelectedProduct} from '../../types/product';

// TODO: updating quantity logic is not working properly
function ProductDisplay({product, selected, updateProduct}: {product: Product, selected: SelectedProduct | undefined, updateProduct: (action: string, product: Product) => void}) {
	return (
		<Box align="center" maxW="sm" p="3" overflow="hidden" key={product.productId.value}>
			<Center h="325px">
				<Image maxH="325px" src={product.imageUrl}></Image>
			</Center>
			<Box h="100px">
				<Text>{product.name}</Text>
				<Stack direction="row" justifyContent="center">
					<Text align="center" color="gray">{product.subtitle}</Text>
				</Stack>
				<Stack direction="row" paddingY="1" justifyContent="center">
					<Text align="center" fontWeight="bold">
						${product.price / product.stepSize}{product.unitType ? `/${product.unitType}` : ''}
					</Text>
				</Stack>
			</Box>

			{selected && selected.quantity > 0?
			(
				<Button w="100%" color="blue.300" variant="outline">
					<MinusIcon onClick={() => updateProduct('remove', product)}></MinusIcon>
					<Box display="inline" w="100%">{selected.quantity}</Box>
					<AddIcon onClick={() => updateProduct('add', product)}></AddIcon>
				</Button>
			) :
			(<Button w="100%" color="blue.300" variant="outline" onClick={() => updateProduct('add', product)}>
				<PlusSquareIcon></PlusSquareIcon>&nbsp;Add to Cart
			</Button>)
			}
		</Box>
	);
};

export default ProductDisplay;
