import {useState} from 'react';
import { Box, Grid } from '@chakra-ui/layout';
import type {Product, SelectedProduct} from '../../types/product';
import ProductDisplay from './ProductDisplay';

function ProductGrid({products, setSelected}: {children: never[], products: Array<Product>, setSelected: (selectedProducts: Array<SelectedProduct>) => void}) {
	const [selectedProducts, setSelectedProducts]: [Array<SelectedProduct>, Function] = useState([]);
	const updateProduct = (action: string, product: Product) => {
		const isSelected = selectedProducts.find(selected => selected.productId === product.productId.value);
		if (isSelected) {
			if (action === 'add') {
				isSelected.quantity += 1;
			} else {
				isSelected.quantity -= 1;
			}
		} else {
			selectedProducts.push({
				productId: product.productId.value,
				quantity: 1
			});
		}

		setSelectedProducts(selectedProducts);
		setSelected(selectedProducts);
	};

	return (
		<Box h="100%">
			<Grid templateColumns="repeat(4, 1fr)" gap={6}>
				{products.map((product: Product) => {
					return (
						<ProductDisplay product={product}
							selected={selectedProducts.find(selected => selected.productId === product.productId.value)} 
							updateProduct={(action, product) => updateProduct(action, product)}></ProductDisplay>
					)
				})}
			</Grid>
		</Box>
	);
};

export default ProductGrid;
